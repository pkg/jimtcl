Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2009, Zachary T Welch zw@superlucidity.net
 2009, Nico Coesel <ncoesel@dealogic.nl>
 2009, David Brownell
 2008, Uwe Klein <uklein@klein-messgeraete.de>
 2008, Steve Bennett <steveb@workware.net.au>
 2008, Duane Ellis <openocd@duaneellis.com>
 2008, Andrew Lunn <andrew@lunn.ch>
 2008, 2009, oharboe - Øyvind Harboe - oyvind.harboe@zylin.com
 2005, patthoyts - Pat Thoyts <patthoyts@users.sf.net>
 2005, Salvatore Sanfilippo <antirez at gmail dot com>
 2005, Clemens Hintze <c.hintze@gmx.net>
License: BSD-2-Clause-Views

Files: autosetup/*
Copyright: 2010, 2011, WorkWare Systems <http://workware.net.au/>
License: BSD-2-Clause-Views

Files: examples.api/* tools/*
Copyright: 2005 Salvatore Sanfilippo <antirez@invece.org>
 2005 Clemens Hintze <c.hintze@gmx.net>
 2005 patthoyts - Pat Thoyts <patthoyts@users.sf.net>
 2008 oharboe - Øyvind Harboe - oyvind.harboe@zylin.com
 2008 Andrew Lunn <andrew@lunn.ch>
 2008 Duane Ellis <openocd@duaneellis.com>
 2008 Uwe Klein <uklein@klein-messgeraete.de>
 2008 Steve Bennett <steveb@workware.net.au>
 2009 Nico Coesel <ncoesel@dealogic.nl>
 2009 Zachary T Welch zw@superlucidity.net
 2009 David Brownell
License: BSD-2-clause

Files: autosetup/autosetup-config.guess
 autosetup/autosetup-config.sub
Copyright: 1992-2021, Free Software Foundation, Inc.
License: GPL-3+ with Autoconf-data exception

Files: debian/*
Copyright: 2024, Bo YU <tsu.yubo@gmail.com>
 2011-2014, Didier Raboud <odyx@debian.org>
 2011, Steve Bennett <steveb@workware.net.au>
 2011, Edgar Grimberg <edgar.grimberg@gmail.com>
License: BSD-2-clause

Files: examples.api/jim_command.c
 examples.api/jim_hello.c
 examples.api/jim_list.c
 examples.api/jim_obj.c
 examples.api/jim_return.c
Copyright: 2010, Wojciech A. Koszek <wkoszek@FreeBSD.org>
License: BSD-2-clause

Files: jim-array.c
 jim-file.c
 jim-namespace.c
 jim-readdir.c
 jim-regexp.c
Copyright: 2008, 2011, Steve Bennett <steveb@workware.net.au>
License: BSD-2-Clause-Views or NTP

Files: jim-exec.c
Copyright: 2008, Steve Bennett <steveb@workware.net.au>
License: NTP

Files: jim-format.c
Copyright: 1999, Scriptics Corporation.
 1995-1997, Sun Microsystems, Inc.
License: BSD-2-Clause-Views

Files: jim-json.c
Copyright: 2019, Steve Bennett <steveb@workware.net.au>
 2015, 2016, Svyatoslav Mishyn <juef@openmailbox.org>
License: ISC

Files: jim-win32.c
Copyright: 2005, Pat Thoyts <patthoyts@users.sourceforge.net>
License: BSD-2-Clause-Views

Files: jim-win32compat.c
Copyright: Salvatore Sanfilippo ,2005.
 Kevlin Henney, 1997, 2003.
License: Kevlin-Henney

Files: jim-zlib.c
Copyright: 2015, 2016, Dima Krasner <dima@dimakrasner.com>
License: BSD-2-Clause-Views

Files: jimsh.c
Copyright: 2009, Steve Bennett <steveb@workware.net.au>
 2005, Salvatore Sanfilippo <antirez at gmail dot com>
License: BSD-2-Clause-Views

Files: jsmn/*
Copyright: 2010, Serge A. Zaitsev
License: Expat

Files: linenoise.h
Copyright: 2010, Salvatore Sanfilippo <antirez at gmail dot com>
 2010, Pieter Noordhuis <pcnoordhuis at gmail dot com>
License: BSD-2-clause

Files: openpty.c
Copyright: 2005-2020, Rich Felker, et al.
License: Expat

Files: EastAsianWidth.txt STYLE Tcl_shipped.html binary.tcl glob.tcl jim-redis.c jim-subcmd.c jim-syslog.c jim-tclprefix.c jim-tty.c jim_tcl.txt jimregexp.c jsonencode.tcl linenoise.c nshelper.tcl parse-unidata.tcl sqlite3/jim-sqlite.c tclcompat.tcl utf8.c utf8.h
Copyright: 2005 Salvatore Sanfilippo <antirez@invece.org>
 2005 Clemens Hintze <c.hintze@gmx.net>
 2005 patthoyts - Pat Thoyts <patthoyts@users.sf.net>
 2008 oharboe - Øyvind Harboe - oyvind.harboe@zylin.com
 2008 Andrew Lunn <andrew@lunn.ch>
 2008 Duane Ellis <openocd@duaneellis.com>
 2008 Uwe Klein <uklein@klein-messgeraete.de>
 2008 Steve Bennett <steveb@workware.net.au>
 2009 Nico Coesel <ncoesel@dealogic.nl>
 2009 Zachary T Welch zw@superlucidity.net
 2009 David Brownell
License: BSD-2-clause

Files: autosetup/autosetup autosetup/cc-db.tcl autosetup/cc-lib.tcl autosetup/cc-shared.tcl autosetup/cc.tcl autosetup/default.auto autosetup/pkg-config.tcl autosetup/system.tcl autosetup/tmake.auto autosetup/tmake.tcl
Copyright: 2006-2011, WorkWare Systems
License: BSD-2-clause

Files: examples/dns.tcl tests/apply.test tests/applyns.test tests/binary.test tests/dict2.test tests/event.test tests/exec.test tests/expr-new.test tests/expr-old.test tests/expr-pow.test tests/for.test tests/format.test tests/glob2.test tests/jim.test tests/linsert.test tests/list.test tests/lock.test tests/lrange.test tests/lreplace.test tests/lsearch.test tests/lsort.test tests/pid.test tests/prefix.test tests/proc.test tests/regexp.test tests/regexp2.test tests/rename.test tests/scan.test tests/string.test tests/stringmatch.test tests/subst.test tests/timer.test tests/uplevel.test tests/upvar.test tests/utftcl.test tests/util.test tests/while.test tests/zlib.test
Copyright: 1991-1994 The Regents of the University of California.
 1994-1998 Sun Microsystems, Inc
 1998-1999 Scriptics Corporation
 1998-2007 Ajuba Solutions
 2001      Kevin B. Kenny
 2002      Pat Thoyts <patthoyts@users.sourceforge.net>
 2003-2009 Donal K. Fellows
 2005-2006 Miguel Sofer
License: TCL
 The following terms apply to all files associated with the software unless
 explicitly disclaimed in individual files.
 .
 The authors hereby grant permission to use, copy, modify, distribute,
 and license this software and its documentation for any purpose, provided
 that existing copyright notices are retained in all copies and that this
 notice is included verbatim in any distributions. No written agreement,
 license, or royalty fee is required for any of the authorized uses.
 Modifications to this software may be copyrighted by their authors
 and need not follow the licensing terms described here, provided that
 the new terms are clearly indicated on the first page of each file where
 they apply.
 .
 IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
 ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
 DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 .
 THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
 IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
 NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
 MODIFICATIONS.
 .
 GOVERNMENT USE: If you are acquiring this software on behalf of the
 U.S. government, the Government shall have only "Restricted Rights"
 in the software and related documentation as defined in the Federal
 Acquisition Regulations (FARs) in Clause 52.227.19 (c) (2).  If you
 are acquiring the software on behalf of the Department of Defense, the
 software shall be classified as "Commercial Computer Software" and the
 Government shall have only "Restricted Rights" as defined in Clause
 252.227-7013 (c) (1) of DFARs.  Notwithstanding the foregoing, the
 authors grant the U.S. Government and others acting in its behalf
 permission to use and distribute the software in accordance with the
 terms specified in this license.
